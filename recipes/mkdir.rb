#
# Cookbook Name:: solr3
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

dir = node[:solr3][:dir]

directory dir do
	recursive true
	action :create
	owner 'root'
	group 'root'
	mode 0700
end