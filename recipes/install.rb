#
# Cookbook Name:: solr3
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

dir = node[:solr3][:dir]
tar = node[:solr3][:tar]
tar_name = node[:solr3][:tar_name]
dest = node[:solr3][:install_dest]
src_dest = node[:solr3][:src_dest]

include_recipe "solr3::mkdir"

remote_file "#{dir}/#{tar_name}.tgz" do
	source tar
	mode 0755
	owner 'root'
	group 'root'
	action :create_if_missing
end

execute 'untar solr3' do
	command "tar zxf #{tar_name}.tgz -C #{src_dest}"
	cwd dir
	creates "#{src_dest}/#{tar_name}"
	action :run
	user 'root'
end

link "#{dest}/solr3" do
	to "#{src_dest}/#{tar_name}"
end