default[:solr3] = {
	:dir => "/var/chef/solr3",
	:tar => "http://ftp.jaist.ac.jp/pub/apache/lucene/solr/3.6.2/apache-solr-3.6.2.tgz",
	:tar_name => "apache-solr-3.6.2",
	:src_dest => "/usr/src",
	:install_dest => "/usr/share"
}